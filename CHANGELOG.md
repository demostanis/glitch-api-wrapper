# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.1](https://gitlab.com/demostanis/glitch-api-wrapper/compare/v0.2.0...v0.2.1) (2020-04-03)

**Note:** Version bump only for package glitch-api-wrapper





# [0.2.0](https://gitlab.com/demostanis/glitch-api-wrapper/compare/v0.1.4...v0.2.0) (2020-04-03)


### Features

* added project functions ([5004c0d](https://gitlab.com/demostanis/glitch-api-wrapper/commit/5004c0da50edb2be449d4c5a9d52319e6aeb9af9))
* added remix() function ([073a7ed](https://gitlab.com/demostanis/glitch-api-wrapper/commit/073a7ed1630524091340daadad4ff53eb212e5c5))





## [0.1.4](https://gitlab.com/demostanis/glitch-api-wrapper/compare/v0.1.3...v0.1.4) (2020-04-02)

**Note:** Version bump only for package glitch-api-wrapper





## [0.1.3](https://gitlab.com/demostanis/glitch-api-wrapper/compare/v0.1.2...v0.1.3) (2020-04-02)


### Bug Fixes

* fixing gitlab ci, again ([9580ade](https://gitlab.com/demostanis/glitch-api-wrapper/commit/9580aded41a1ae8ef2eb8407725057cbbdfbf2a1))





## [0.1.2](https://gitlab.com/demostanis/glitch-api-wrapper/compare/v0.1.1...v0.1.2) (2020-04-02)


### Bug Fixes

* checking if gitlab ci works ([a41eb63](https://gitlab.com/demostanis/glitch-api-wrapper/commit/a41eb6311e8a67e1344755e1d700abca7ba2d42a))
* i believe it's working ([8a44b26](https://gitlab.com/demostanis/glitch-api-wrapper/commit/8a44b26025a54814ce33dcb8a7e73e3aa897657c))





## [0.1.1](https://gitlab.com/demostanis/glitch-api-wrapper/compare/v0.1.0...v0.1.1) (2020-04-02)

**Note:** Version bump only for package glitch-api-wrapper





# [0.1.0](https://gitlab.com/demostanis/glitch-api-wrapper/compare/v0.0.1...v0.1.0) (2020-04-02)


### Bug Fixes

* added --canary option with lerna publish ([de600f5](https://gitlab.com/demostanis/glitch-api-wrapper/commit/de600f5ed4871d68ddff2c3177c88331798ce397))
* ci only publishes on master ([38acdfb](https://gitlab.com/demostanis/glitch-api-wrapper/commit/38acdfb2e389e90df061549203b90dbf715b521a))
* done some changes for publishing ([cac56f2](https://gitlab.com/demostanis/glitch-api-wrapper/commit/cac56f2cd6345ffc3eff070c3db3fa5933512eb7))
* fixed lerna wanting a confirmation ([5006d01](https://gitlab.com/demostanis/glitch-api-wrapper/commit/5006d01c720dc251f8048fabbcc96e5cc4e07d0d))
* fixed publish job ([0ad5499](https://gitlab.com/demostanis/glitch-api-wrapper/commit/0ad5499d0b69f63f8cc57b4dedf4438292bb0402))


### Features

* added registerAnon function ([323414f](https://gitlab.com/demostanis/glitch-api-wrapper/commit/323414f57ea3e14c5e7141ed00b303849c01864f))





## 0.0.1 (2020-04-01)

**Note:** Version bump only for package glitch-api-wrapper
