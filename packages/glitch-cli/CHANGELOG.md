# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.1](https://gitlab.com/demostanis/glitch-api-wrapper/compare/v0.2.0...v0.2.1) (2020-04-03)

**Note:** Version bump only for package @glitch-api-wrapper/cli





# [0.2.0](https://gitlab.com/demostanis/glitch-api-wrapper/compare/v0.1.4...v0.2.0) (2020-04-03)

**Note:** Version bump only for package @glitch-api-wrapper/cli





## [0.1.1](https://gitlab.com/demostanis/glitch-api-wrapper/compare/v0.1.0...v0.1.1) (2020-04-02)

**Note:** Version bump only for package @glitch-api-wrapper/cli





# [0.1.0](https://gitlab.com/demostanis/glitch-api-wrapper/compare/v0.0.1...v0.1.0) (2020-04-02)

**Note:** Version bump only for package @glitch-api-wrapper/cli





## 0.0.1 (2020-04-01)

**Note:** Version bump only for package @glitch-api-wrapper/cli
