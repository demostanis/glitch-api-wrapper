import glitch from ".."
import User from "../user"
import Project from "../project"

it("should create an anonymous user", () => {
  return glitch.registerAnon().then(user => {
    expect(user).toBeInstanceOf(User)
    expect(user.persistentToken).toBeDefined()
    expect(user.id).toBeDefined()
  })
})

it("should create a blank project", () => {
  return glitch.remix().then(project => {
    expect(project).toBeInstanceOf(Project)
  })
})

it("should get more info about the project", () => {
  return glitch.projects
    .slice()
    .pop()
    .data()
    .then(data => {
      expect(data).toBeTruthy()
    })
})

it("should delete the project", () => {
  return glitch.projects
    .slice()
    .pop()
    .delete()
    .then(data => {
      expect(data).toBeTruthy()
    })
})
