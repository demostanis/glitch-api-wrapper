import got from "got"
import User from "./user"
import Project from "./project"

class GlitchApiWrapper {
  currentUser = null
  projects = []
  _cookie = null

  constructor(
    options = {
      prefixUrl: "https://api.glitch.com/",
      userAgent: "GlitchApiWrapper/0.1"
    }
  ) {
    this.client = got.extend({
      prefixUrl: options.prefixUrl,
      headers: {
        "User-Agent": options.userAgent
      },
      hooks: {
        beforeRequest: [
          options => {
            if (this.currentUser && this.currentUser.persistentToken) {
              options.headers[
                "Authorization"
              ] = this.currentUser.persistentToken
            }
            if (this._cookie) {
              options.headers["Cookie"] = this._cookie
            }
          }
        ]
      }
    })
  }

  /**
   * Used to register an anonymous user
   * @returns Promise<User>
   */
  registerAnon() {
    return new Promise((resolve, reject) => {
      this.client
        .post("v1/users/anon")
        .then(data => {
          const user = JSON.parse(data.body)
          const [cookie] = data.headers["set-cookie"]
            .shift()
            .match(/GlitchAuth=.*;/)

          if (cookie) {
            this._cookie = cookie
          } else {
            reject(new Error("No auth token was provided by Glitch"))
            return
          }

          this.currentUser = new User(user)
          resolve(this.currentUser)
        })
        .catch(reject)
    })
  }

  /**
   * Used to remix a project
   *  @returns Promise<Project>
   */
  remix(id = "project-with-nothing") {
    return new Promise((resolve, reject) => {
      if (!this._cookie) {
        reject(
          new Error("It is required to call registerAnon() before remix()")
        )
        return
      }

      this.client
        .post(`v1/projects/by/domain/${id}/remix`)
        .json()
        .then(data => {
          const project = new Project(data)

          this.projects.push(project)
          resolve(project)
        })
        .catch(reject)
    })
  }
}

// export an instance of GlitchApiWrapper,
// as well as the constructor and its alias

export default new GlitchApiWrapper()
export { GlitchApiWrapper as Glitch }
export { GlitchApiWrapper }
