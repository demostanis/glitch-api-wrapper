import glitch from ".."

class Project {
  constructor(options) {
    this.id = options.id
    this.description = options.description
    this.domain = options.domain

    this._raw = options
  }

  /**
   * Used to get more info about the project
   * like invite token, number of visitors...
   * @returns Promise
   */
  data() {
    return new Promise((resolve, reject) => {
      glitch.client
        .get(`v1/projects/by/id?id=${this.id}`)
        .json()
        .then(resolve)
        .catch(reject)
    })
  }

  /**
   * Used to delete this project
   * @returns Promise
   */
  delete() {
    return new Promise((resolve, reject) => {
      glitch.client
        .delete(`v1/projects/${this.id}`)
        .json()
        .then(data => {
          if (data.domain.endsWith("-deleted")) {
            resolve(data)
          } else {
            reject(new Error("Project was not correctly deleted", data))
          }
        })
        .catch(reject)
    })
  }
}

export default Project
