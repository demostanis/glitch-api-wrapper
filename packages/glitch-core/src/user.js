class User {
  constructor(options) {
    this.persistentToken = options.persistentToken
    this.id = options.id
    this.githubId = options.githubId
    this.githubToken = options.githubToken
    this.name = options.name
    this.facebookId = options.facebookId
    this.facebookToken = options.facebookToken
    this.description = options.description
    this.googleId = options.googleId
    this.googleToken = options.googleToken
    this.slackId = options.slackId
    this.slackToken = options.slackToken
    this.slackTeamId = options.slackTeamId

    this._raw = options
  }
}

export default User
