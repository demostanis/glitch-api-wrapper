module.exports = {
  clearMocks: true,
  coverageReporters: ["text"],
  roots: ["<rootDir>packages"],
  setupFilesAfterEnv: ["<rootDir>/setupTests.js"],
  transform: {
    "^.+\\.[t|j]sx?$": "babel-jest"
  }
}
